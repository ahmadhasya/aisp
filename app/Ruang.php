<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruang extends Model
{
    protected $table = "tbl_ruang";
    protected $primaryKey = "id_ruang";
    protected $guarded = ["id_ruang"];
    public $timestamps = false;

    public static function addRuang($r){
        $r->validate([
            "kodeRuang" => "required",
            "namaRuang" => "required",
            "keteranganRuang" => "required"
        ]);
        $data = new Ruang;
        if($r->idRuang != null){
            $data = Ruang::find($r->idRuang);
        }
        $data->kode_ruang = $r->kodeRuang;
        $data->nama_ruang = $r->namaRuang;
        $data->keterangan = $r->keteranganRuang;
        $data->save();
        
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function deleteRuang($id){
        $data = new Ruang;
        $data = Ruang::findOrFail($id);
        
        Ruang::destroy($id);
        
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function allRuang(){
        $data = Ruang::orderBy("nama_ruang")->get();

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function getRuang($id){
        $data = Ruang::find($id);

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }
}
