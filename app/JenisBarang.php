<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBarang extends Model
{
    protected $table = "tbl_jenis";
    protected $primaryKey = "id_jenis";
    protected $guarded = ["id_jenis"];
    public $timestamps = false;

    public static function addJenisBarang($r){
        $r->validate([
            "kodeJenis" => "required",
            "namaJenis" => "required",
            "keteranganJenis" => "required"
        ]);
        $data = new JenisBarang;
        if($r->idJenis != null){
            $data = JenisBarang::find($r->idJenis);
        }
        $data->kode_jenis = $r->kodeJenis;
        $data->nama_jenis = $r->namaJenis;
        $data->keterangan = $r->keteranganJenis;
        $data->save();
        
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function deleteJenisBarang($id){
        $data = new JenisBarang;
        $data = JenisBarang::findOrFail($id);
        
        JenisBarang::destroy($id);
        
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function allJenisBarang(){
        $data = JenisBarang::orderBy("nama_jenis")->get();

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function getJenisBarang($id){
        $data = JenisBarang::find($id);

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }
}
