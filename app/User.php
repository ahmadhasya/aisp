<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="tbl_petugas";
    protected $primaryKey="id_petugas";
    protected $fillable = [
        'nama_petugas', 'username', 'password',"id_level"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',"remember_token"
    ];

    public $timestamps=false;
    public function level(){
        return $this->belongsTo('App\Level',"id_level");
    }

    public function findForPassport($username) {
        return $this->where("username", $username)->first();
    }
}
