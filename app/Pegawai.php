<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Pegawai extends Model
{
	protected $table = "tbl_pegawai";
	protected $primaryKey = "id_pegawai";
	protected $guarded = ["id_pegawai"];
	public $timestamps = false;

	public static function addPegawai($r){
		$data = DB::select("call addPegawai('".$r->NIPPegawai."','".$r->NamaPegawai."','".$r->AlamatPegawai."')")[0];
		return $data;
	}

	public static function allPegawai(){
		$data = Pegawai::all();
		$response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
	}
}
