<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventaris;
use App\Pegawai;
use App\Peminjaman;
use App\DetailPinjam;
use Illuminate\Support\Facades\Auth;
use DB;

class CTRLApiPeminjaman extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return view('home');
    }

    public function allPeminjaman(){
    	return Peminjaman::allPeminjaman();
    }

    public function allPengembalian()
    {
	if(Pegawai::where("nip",Auth::user()->nip)->first()!=null){
		$data = Peminjaman::with(["detail" => function($query){
            $query->with("inventaris");
        }])
        ->where("id_pegawai",Pegawai::where("nip",Auth::user()->nip)->first()->id_pegawai)
        ->where("status_peminjaman",0)
        ->get();
	}else{
		$data = [];
	}
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public function getPeminjaman($id){
        return Inventaris::getInventaris($id);
    }

    public function addPeminjaman(Request $r){
        $r->NamaPegawai = Auth::user()->nama_petugas;
		$r->NIPPegawai = Auth::user()->nip;
		$r->AlamatPegawai = Auth::user()->alamat;
        $pegawai    = Pegawai::addPegawai($r);
        $r->Pegawai = $pegawai->id_pegawai;
        $peminjaman = Peminjaman::addPeminjaman($r);
        DetailPinjam::addDetailPinjam($r,$peminjaman);
        $response["status"] = 200;
        $response["data"]   = $pegawai;
        return response()->json($response);
    }
}
