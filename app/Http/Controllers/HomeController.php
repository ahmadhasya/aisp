<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()){
            return view("app");
        }else{
            return view("login");
        }
    }
    public function getUser(){
        $data["user"] = User::with("level")->find(Auth::user()->id_petugas);
        if($data["user"]->level->nama_level == "Admin"){
            $data["link"]="inventarisir";
        }else{
            $data["link"]="peminjaman";
        }
        return $data;
    }
}
