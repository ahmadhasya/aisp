<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventaris;
use App\Pegawai;
use App\Peminjaman;
use App\DetailPinjam;

class CTRLPeminjaman extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allPeminjaman(){
    	return Peminjaman::allPeminjaman();
    }

    public function allPegawai(){
    	return Pegawai::allPegawai();
    }

    public function getPeminjaman($id){
        return Inventaris::getInventaris($id);
    }

    public function addPeminjaman(Request $r){
        $peminjaman = Peminjaman::addPeminjaman($r);
        DetailPinjam::addDetailPinjam($r,$peminjaman);
        $response["status"] = 200;
        $response["data"]   = $r->input();
        return response()->json($response);
    }
}
