<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPinjam;
use App\Peminjaman;

class CTRLPengembalian extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('notpeminjam');
    }

    public function allPengembalian()
    {
        return DetailPinjam::allPengembalian();
    }

    public function Pengembalian($id)
    {
        return Peminjaman::Pengembalian($id);
    }

    public function getPengembalian($id)
    {
        return Peminjaman::getPengembalian($id);
    }
}
