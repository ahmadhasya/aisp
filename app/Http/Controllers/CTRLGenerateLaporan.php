<?php

namespace App\Http\Controllers;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use App\Inventaris;
use App\JenisBarang;
use App\Ruang;
use App\DetailPinjam;
use App\Pegawai;
use Illuminate\Support\Facades\Auth;

class CTRLGenerateLaporan extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin');
	}

	public function generateLaporan()
	{
		$pathFile = storage_path() . '/Laporan AISP.pdf';
		$pdf = new Fpdf;
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial', 'B', 16);
		// mencetak string 
		$pdf->Cell(190, 7, 'SEKOLAH MENENGAH KEJURUAN NEGERI 1 CIMAHI', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 7, 'LAPORAN INVENTARIS SARANA DAN PRASARANA SMK', 0, 1, 'C');
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(190, 7, 'Tanggal : ' . date("d-m-Y") . ' Pukul : ' . date("G:i"), 0, 1, 'C');

		// Memberikan space kebawah agar tidak terlalu rapat
		$pdf->Cell(10, 7, '', 0, 1);

		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Data Inventaris", 0, 1, 'C');

		$pdf->Cell(8, 6, 'No.', 1, 0);
		$pdf->Cell(30, 6, 'Kode Inventaris', 1, 0);
		$pdf->Cell(45, 6, 'Nama', 1, 0);
		$pdf->Cell(20, 6, 'Jumlah', 1, 0);
		$pdf->Cell(27, 6, 'Kode Jenis', 1, 0);
		$pdf->Cell(25, 6, 'Kode Ruang', 1, 0);
		$pdf->Cell(35, 6, 'Tanggal Register', 1, 1);

		$pdf->SetFont('Arial', '', 10);
		$inventaris = Inventaris::allInventaris();
		$no = 0;
		foreach ($inventaris->getData()->data as $value) {
			$pdf->Cell(8, 6, ++$no, 1, 0);
			$pdf->Cell(30, 6, $value->kode_inventaris, 1, 0);
			$pdf->Cell(45, 6, $value->nama, 1, 0);
			$pdf->Cell(20, 6, $value->jumlah, 1, 0);
			$pdf->Cell(27, 6, $value->jenis->kode_jenis, 1, 0);
			$pdf->Cell(25, 6, $value->ruang->kode_ruang, 1, 0);
			$pdf->Cell(35, 6, $value->tanggal_register, 1, 1);
		}

		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Data Ruang", 0, 1, 'C');

		$pdf->Cell(8, 6, 'No.', 1, 0);
		$pdf->Cell(40, 6, 'Kode Ruang', 1, 0);
		$pdf->Cell(75, 6, 'Nama Ruang', 1, 0);
		$pdf->Cell(67, 6, 'Keterangan', 1, 1);

		$pdf->SetFont('Arial', '', 10);
		$Ruang = Ruang::allRuang();
		$no = 0;
		foreach ($Ruang->getData()->data as $value) {
			$pdf->Cell(8, 6, ++$no, 1, 0);
			$pdf->Cell(40, 6, $value->kode_ruang, 1, 0);
			$pdf->Cell(75, 6, $value->nama_ruang, 1, 0);
			$pdf->Cell(67, 6, $value->keterangan, 1, 1);
		}

		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Data Jenis Barang", 0, 1, 'C');

		$pdf->Cell(8, 6, 'No.', 1, 0);
		$pdf->Cell(40, 6, 'Kode Jenis Barang', 1, 0);
		$pdf->Cell(75, 6, 'Nama Jenis Barang', 1, 0);
		$pdf->Cell(67, 6, 'Keterangan', 1, 1);

		$pdf->SetFont('Arial', '', 10);
		$JenisBarang = JenisBarang::allJenisBarang();
		$no = 0;
		foreach ($JenisBarang->getData()->data as $value) {
			$pdf->Cell(8, 6, ++$no, 1, 0);
			$pdf->Cell(40, 6, $value->kode_jenis, 1, 0);
			$pdf->Cell(75, 6, $value->nama_jenis, 1, 0);
			$pdf->Cell(67, 6, $value->keterangan, 1, 1);
		}

		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Data Pegawai", 0, 1, 'C');

		$pdf->Cell(8, 6, 'No.', 1, 0);
		$pdf->Cell(40, 6, 'Nomor Induk Pegawai', 1, 0);
		$pdf->Cell(60, 6, 'Nama Pegawai', 1, 0);
		$pdf->Cell(82, 6, 'Alamat', 1, 1);

		$pdf->SetFont('Arial', '', 10);
		$no = 0;
		foreach (Pegawai::all() as $value) {
			$pdf->Cell(8, 6, ++$no, 1, 0);
			$pdf->Cell(40, 6, $value->nip, 1, 0);
			$pdf->Cell(60, 6, $value->nama_pegawai, 1, 0);
			$pdf->Cell(82, 6, $value->alamat, 1, 1);
		}
		$pdf->Cell(10, 14, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Tertanda", 0, 1, 'R');
		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->Cell(190, 7, Auth::user()->nama_petugas." (". Auth::user()->level->nama_level.")", 0, 1, 'R');

		$pdf->Output('F', $pathFile);
		$headers = [
			'Content-Type' => 'application/pdf'
		];

		return response()->file($pathFile, $headers);
	}

	public function laporanPeminjaman($mulai,$akhir)
	{
		$pathFile = storage_path() . '/Laporan AISP.pdf';
		$pdf = new Fpdf;
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial', 'B', 16);
		// mencetak string 
		$pdf->Cell(190, 7, 'SEKOLAH MENENGAH KEJURUAN NEGERI 1 CIMAHI', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 7, 'LAPORAN PEMINJAMAN SARANA DAN PRASARANA SMK', 0, 1, 'C');
		$pdf->Cell(190, 7,"Pada : ".  $mulai ." s/d ". $akhir, 0, 1, 'C');
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(190, 7, 'Tanggal : ' . date("Y-m-d") .' Pukul : ' . date("G:i"), 0, 1, 'C');

		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Data Peminjaman", 0, 1, 'C');

		$pdf->Cell(8, 6, 'No.', 1, 0);
		$pdf->Cell(50, 6, 'Kode Inventaris (Jumlah)', 1, 0);
		$pdf->Cell(40, 6, 'NIP Pegawai', 1, 0);
		$pdf->Cell(30, 6, 'Tanggal Pinjam', 1, 0);
		$pdf->Cell(30, 6, 'Tanggal Kembali', 1, 0);
		$pdf->Cell(32, 6, 'Status', 1, 1);

		$pdf->SetFont('Arial', '', 10);
		$JenisBarang = DetailPinjam::allPengembalianReport($mulai,$akhir);
		$no = 0;
		foreach ($JenisBarang->getData()->data as $value) {
			$height = 6 *count($value->inventaris);
			$y1 = $pdf->GetY();
			$pdf->MultiCell(8, $height,++$no , 1);
			$pdf->SetXY(18, $y1);
			$kode_inventaris = "";
			foreach ($value->inventaris as $key => $Inventaris) {
				$kode_inventaris = $kode_inventaris . "$Inventaris->kode_inventaris (" . $value->detail[$key]->jumlah . "),\n";
			}
			$pdf->MultiCell(50, 6, $kode_inventaris, 1, 1);
			$pdf->SetXY(68, $y1);

			$pdf->MultiCell(40, $height, $value->pegawai->nip, 1, 1);
			$pdf->SetXY(108, $y1);

			$pdf->MultiCell(30, $height, $value->tanggal_pinjam, 1, 1);
			$pdf->SetXY(138, $y1);

			$pdf->MultiCell(30, $height, $value->tanggal_kembali, 1, 1);
			$pdf->SetXY(168, $y1);

			$pdf->MultiCell(32, $height, $value->status_peminjaman == 1 ?"Dikembalikan" : "Belum Kembali", 1);
		}

		$pdf->Cell(10, 14, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(190, 7, "Tertanda", 0, 1, 'R');
		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->Cell(190, 7, Auth::user()->nama_petugas." (". Auth::user()->level->nama_level.")", 0, 1, 'R');

		$pdf->Output('F', $pathFile);
		$headers = [
			'Content-Type' => 'application/pdf'
		];

		return response()->file($pathFile, $headers);
	}
}
