<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventaris;
use App\JenisBarang;
use App\Ruang;

class CTRLInventarisir extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function allInventaris(){
        return Inventaris::allInventaris();
    }

    public function addInventaris(Request $r){
        return Inventaris::addInventaris($r);
    }

    public function deleteInventaris($id){
        return Inventaris::deleteInventaris($id);
    }

    public function getInventaris($id){
        return Inventaris::getInventaris($id);
    }

    public function allJenisBarang(){
        return JenisBarang::allJenisBarang();
    }

    public function addJenisBarang(Request $r){
        return JenisBarang::addJenisBarang($r);
    }

    public function deleteJenisBarang($id){
        return JenisBarang::deleteJenisBarang($id);
    }

    public function getJenisBarang($id){
        return JenisBarang::getJenisBarang($id);
    }


    public function addRuang(Request $r){
        return Ruang::addRuang($r);
    }

    public function deleteRuang($id){
        return Ruang::deleteRuang($id);
    }

    public function getRuang($id){
        return Ruang::getRuang($id);
    }

    public function allRuang(){
        return Ruang::allRuang();
    }
}
