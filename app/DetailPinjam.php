<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Peminjaman;
use DB;

class DetailPinjam extends Model
{
    protected $table = "tbl_detail_pinjam";
    protected $primaryKey = "id_detail_pinjam";
    protected $guarded = [];
    public $timestamps = false;

    public static function addDetailPinjam($r,$peminjaman){
        $r->validate(["Inventaris"=>"required"]);
        foreach($r->input("Inventaris") as $data){
            DetailPinjam::create([
                "id_detail_pinjam" => $peminjaman->id_peminjaman,
                "id_inventaris"    => $data["Inventaris"]["id_inventaris"],
                "jumlah"		   => $data["JumlahPinjam"]
            ]);
        }
    }

    public static function allPengembalian(){
        $data = Peminjaman::withCount(['detail as macam','detail as jumlah' => function($query) {
            $query->select(DB::raw('sum(jumlah)'));
        }])->with(["pegawai","detail","inventaris"])->get();
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function allPengembalianReport($mulai,$akhir){
        $data = Peminjaman::withCount(['detail as macam','detail as jumlah' => function($query) {
            $query->select(DB::raw('sum(jumlah)'));
        }])->with(["pegawai","detail","inventaris"])->whereBetween("tanggal_pinjam",[$mulai,$akhir])->get();
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public function pinjam(){
        return $this->hasOne("App\Peminjaman","id_peminjaman","id_detail_pinjam");
    }

    public function pegawai(){
        return $this->hasManyThrough("App\Pegawai","App\Peminjaman","id_peminjaman","id_pegawai","id_detail_pinjam","id_pegawai");
    }

    public function inventaris(){
        return $this->belongsTo("App\Inventaris","id_inventaris");
    }
}
