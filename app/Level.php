<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table="tbl_level";
    protected $primaryKey="id_level";
}
