<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Inventaris;
use App\DetailPinjam;
use Illuminate\Support\Facades\DB;

class Peminjaman extends Model
{
    protected $table = "tbl_peminjaman";
    protected $primaryKey = "id_peminjaman";
    protected $guarded = ["id_peminjaman"];
    public $timestamps = false;

    public static function addPeminjaman($r){
        $r->validate([
            "TanggalKembali" => "required",
            "TanggalPinjam" => "required"
        ]);
        // $data = DB::select("call addPeminjaman('".substr($r->TanggalPinjam,0,10)."','".substr($r->TanggalKembali,0,10)."','". $pegawai->id_pegawai."')");
    	$data = Peminjaman::create([
    		"tanggal_pinjam" => substr($r->TanggalPinjam,0,10),
            "tanggal_kembali" => substr($r->TanggalKembali,0,10),
    		"id_pegawai"	 => $r->Pegawai
    	]);
    	return $data;
    }

    public static function allPeminjaman(){
        $data = Inventaris::all();
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function Pengembalian($id){
        $data = Peminjaman::findOrFail($id);
        $data->update([
            "status_peminjaman" => 1
        ]);
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);   
    }

    public static function getPengembalian($id){
        $data = Peminjaman::with(["inventaris","pegawai","detail"])->findOrFail($id);
        
        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public function pegawai(){
        return $this->belongsTo("App\Pegawai","id_pegawai");
    }

    public function detail(){
        return $this->hasMany("App\DetailPinjam","id_detail_pinjam","id_peminjaman");
    }

    public function inventaris(){
        return $this->hasManyThrough("App\Inventaris","App\DetailPinjam","id_detail_pinjam","id_inventaris","id_peminjaman","id_inventaris");
    }
}
