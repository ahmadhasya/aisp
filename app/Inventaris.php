<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;

class Inventaris extends Model
{
    protected $table = "tbl_inventaris";
    protected $primaryKey = "id_inventaris";
    protected $guarded = ["id_inventaris"];
    public $timestamps = false;

    public static function addInventaris($r)
    {
        $data = new Inventaris;
        // $r->validate([
        //     "Jenis" => "required",
        //     "Ruang" => "required",
        //     "Jumlah" => "required",
        //     "keteranganInvenarisir" => "required",
        //     "Kondisi" => "required",
        //     "kodeInventarisir" => "required",
        //     "namaInventarisir" => "required",
        //     "KeteranganInventarisir" => "required"
        // ]);
        if ($r->idInventaris != null) {
            $data = Inventaris::find($r->idInventaris);
        }

        $data->id_jenis         = $r->Jenis;
        $data->id_ruang         = $r->Ruang;
        $data->jumlah           = $r->Jumlah;
        $data->keterangan       = $r->keteranganInventarisir;
        $data->kondisi          = $r->Kondisi;
        $data->kode_inventaris  = $r->kodeInventarisir;
        $data->nama             = $r->namaInventarisir;
        $data->keterangan       = $r->keteranganInventarisir;
        $data->tanggal_register = date("Y-m-d");
        $data->id_petugas       = Auth::user()->id_petugas;
        $data->save();

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function deleteInventaris($id)
    {
        $data = new Inventaris;
        $data = Inventaris::findOrFail($id);

        Inventaris::destroy($id);

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function allInventaris()
    {
        $data = Inventaris::with("ruang")->with("jenis")->orderBy("nama")->get();

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public static function getInventaris($id)
    {
        $data = Inventaris::with(["detail_pinjam" => function ($query) {
            $query->whereHas("pinjam", function ($querinner) {
                $querinner->where("status_peminjaman", 0);
            });
        }, "ruang", "jenis", 'peminjaman' => function ($query) {
            $query->with("pegawai")->where("status_peminjaman", 0);
        }])->withCount(['peminjaman as terpinjam' => function ($query) {
            $query->select(DB::raw('sum(jumlah)'))->where("status_peminjaman", 0);
        }])->findOrFail($id);

        $response["status"] = 200;
        $response["data"]   = $data;
        return response()->json($response);
    }

    public function ruang()
    {
        return $this->belongsTo('App\Ruang', "id_ruang", "id_ruang");
    }

    public function jenis()
    {
        return $this->belongsTo('App\JenisBarang', "id_jenis", "id_jenis");
    }

    public function peminjaman()
    {
        return $this->hasManyThrough("App\Peminjaman", "App\DetailPinjam", "id_inventaris", "id_peminjaman", "id_inventaris", "id_detail_pinjam");
    }

    public function detail_pinjam()
    {
        return $this->hasMany("App\DetailPinjam", "id_inventaris");
    }
}
