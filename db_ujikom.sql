-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 11 Apr 2019 pada 07.27
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ujikom`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addPegawai` (IN `innip` VARCHAR(50), IN `innama` VARCHAR(50), IN `inalamat` TEXT)  BEGIN
    IF NOT EXISTS(SELECT * FROM tbl_pegawai WHERE `nip` = innip)
		THEN
            Insert into tbl_pegawai(`nip`,`nama_pegawai`,`alamat`) VALUES(innip,innama,inalamat);
        END IF;
        Select * from tbl_pegawai where `nip` = innip;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addPeminjaman` (IN `pinjam` DATE, IN `kembali` DATE, IN `pegawai` INT(10))  BEGIN
      Insert into tbl_peminjaman(`tanggal_pinjam`,`tanggal_kembali`,`id_pegawai`) VALUES(pinjam,kembali,pegawai);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('03926b65d877c320e85d028d09667b50bdf37b6a7b075dac437354dba7563755f2002d7cc0e84ca6', 2, 3, NULL, '[]', 0, '2019-04-05 03:08:07', '2019-04-05 03:08:07', '2020-04-05 10:08:07'),
('18018a10bb23935b8f561c40ef9afb3beaeb6ebe8dfa91ac12ae71be5f7a905f4bd03f7d182ec406', 4, 3, NULL, '[]', 0, '2019-04-05 07:35:07', '2019-04-05 07:35:07', '2020-04-05 14:35:07'),
('3fe6f02b26df37f9200a2aba34d0f552906492e0fb5d7f0fbe66a08c3166f276a4b5805e2ea0506d', 2, 3, NULL, '[]', 0, '2019-04-05 03:29:12', '2019-04-05 03:29:12', '2020-04-05 10:29:12'),
('421dc770e1043410fafb772e80a9d3de6577da52ea0bc2a6754f3b7babdf04948439249c7f799575', 2, 3, NULL, '[]', 0, '2019-04-06 01:12:26', '2019-04-06 01:12:26', '2020-04-06 08:12:26'),
('5126977d97d9290b87d21ede8d3358bf39f4f7071e0e569e63a8c1c66eb8229cc368f40b9cfb7425', 4, 3, NULL, '[]', 0, '2019-04-05 06:24:07', '2019-04-05 06:24:07', '2020-04-05 13:24:07'),
('6c95f35be6f0ec832c0200539ee09d39afc71087e494c0abe6f4a31ee0c98339959c8729e1f92bf2', 2, 3, NULL, '[]', 0, '2019-04-05 03:12:37', '2019-04-05 03:12:37', '2020-04-05 10:12:37'),
('80f6ab6028d4028958dddd3d8d20fa78e9895fe2ef8df1e128effe1f5b7be6771ff563ccbdfc6855', 2, 3, NULL, '[]', 0, '2019-04-05 01:48:57', '2019-04-05 01:48:57', '2020-04-05 08:48:57'),
('831b0c8194b83d656f07312db7887f9877bc54c3e7085ccb7e71c743acd228ff7fbd07ddf00f9d99', 3, 2, NULL, '[]', 0, '2019-04-05 03:15:48', '2019-04-05 03:15:48', '2020-04-05 10:15:48'),
('925bca96dddd19fb0bc77b5976e87527d1e0ae3e2a3d0612f19eb6917122f0ba2cc91e22e2b70f7e', 3, 3, NULL, '[]', 0, '2019-04-05 03:12:48', '2019-04-05 03:12:48', '2020-04-05 10:12:48'),
('a79ded06519d1d64eb86601d3d2d1bdf5dc405dbf867e7acc18b954ed8a75900958525b614c92f2b', 3, 3, NULL, '[]', 0, '2019-04-05 03:08:16', '2019-04-05 03:08:16', '2020-04-05 10:08:16'),
('a975d13f605bbecaa8929cbcba79fa85ea39355ba03cfb2cfbbe5acc6d01e30eb022ae5a44d76439', 2, 3, NULL, '[]', 0, '2019-04-05 07:36:43', '2019-04-05 07:36:43', '2020-04-05 14:36:43'),
('c41afc61cbd2532f99e5a87a8238e1fcfa318abfbeee520e58322b37ceb63124ea6b55751c937077', 2, 3, NULL, '[]', 0, '2019-04-05 07:36:53', '2019-04-05 07:36:53', '2020-04-05 14:36:53'),
('eaeb297c332259c1f69ed32f94f003407efcdc3b399acd90692dab45fe23c9c2ea317fdeb23d3162', 2, 3, NULL, '[]', 0, '2019-04-05 03:20:24', '2019-04-05 03:20:24', '2020-04-05 10:20:24'),
('ec723a0ae60cab16089cc9431e6b47f6f93e1aa1b8016d3f4a04f13886c2640ff7ef666da0c62ae6', 3, 3, NULL, '[]', 0, '2019-04-05 03:14:09', '2019-04-05 03:14:09', '2020-04-05 10:14:09'),
('eec26f1b7eeec9d0178f6307cd3b9a76740f494b75f00d45bfc7ed05df66226a9d3a30f578c05f27', 3, 3, NULL, '[]', 0, '2019-04-05 03:20:15', '2019-04-05 03:20:15', '2020-04-05 10:20:15'),
('f66ba099cea68ea831ce4e515430e0f87badaa27d11b074b0429497c12f2e86edd21439fc9e90a22', 3, 3, NULL, '[]', 0, '2019-04-05 02:01:55', '2019-04-05 02:01:55', '2020-04-05 09:01:55'),
('f74e0d18da10346227b59f35868df376a630e4ebdcfe0084fc09430025fea290bb631ce1681a6de4', 2, 3, NULL, '[]', 0, '2019-04-06 01:12:27', '2019-04-06 01:12:27', '2020-04-06 08:12:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'hAyMD81D3Qd0qGU0y6HTYHRhTTVkPoRxnnf9p9nD', 'http://localhost', 1, 0, 0, '2019-03-28 02:13:34', '2019-03-28 02:13:34'),
(2, NULL, 'Laravel Password Grant Client', 'I2dBT1UM4hb1oBQ9qtCTEnZxGtBfFu6rHB0GJALF', 'http://localhost', 0, 1, 0, '2019-03-28 02:13:34', '2019-03-28 02:13:34'),
(3, NULL, 'password', 'bhV3ft95V2lTW46chAThx3KCyodvHnUzujaXxF9S', 'http://localhost', 0, 1, 0, '2019-03-29 04:04:51', '2019-03-29 04:04:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-28 02:13:34', '2019-03-28 02:13:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('1a8097190dd4137e67e49bdca10d436bb1b869c3b214cda57664a7d57d5561da6ae34077e9087fcc', 'c41afc61cbd2532f99e5a87a8238e1fcfa318abfbeee520e58322b37ceb63124ea6b55751c937077', 0, '2020-04-05 14:36:53'),
('29797dbc88da4e46451c9a5462ad69a438e2bf11dd0a3e44b1d8ad071686b7bf63aadc955403d7f3', 'f66ba099cea68ea831ce4e515430e0f87badaa27d11b074b0429497c12f2e86edd21439fc9e90a22', 0, '2020-04-05 09:01:55'),
('41f66fcaa4cf59f4d1a394102d5b6312faf24663d5d0ef2dd22a9874e0ec17bb368656b938ba1598', 'f74e0d18da10346227b59f35868df376a630e4ebdcfe0084fc09430025fea290bb631ce1681a6de4', 0, '2020-04-06 08:12:27'),
('46190b67020e620a913594d300f238f137542d944e2beec68602927be3c4d59e3d79c811c0f29f01', '925bca96dddd19fb0bc77b5976e87527d1e0ae3e2a3d0612f19eb6917122f0ba2cc91e22e2b70f7e', 0, '2020-04-05 10:12:48'),
('4866b84d6922d4248f828ab4c99473d0dfbc7081979acc3a31f03d21f6b3e369790a7c02ba277b47', '6c95f35be6f0ec832c0200539ee09d39afc71087e494c0abe6f4a31ee0c98339959c8729e1f92bf2', 0, '2020-04-05 10:12:37'),
('4ed316b6e56a9a9bc145fa3dd44b6299c29361b6fc43faadbc2bb437d7fce5bfdb5cecf02a2296c7', 'ec723a0ae60cab16089cc9431e6b47f6f93e1aa1b8016d3f4a04f13886c2640ff7ef666da0c62ae6', 0, '2020-04-05 10:14:09'),
('5348a39e6dac246e3889a3442f07a0127715d41230286e1a0b16a6fb6c7dad2772b35006e7b1d7e7', '03926b65d877c320e85d028d09667b50bdf37b6a7b075dac437354dba7563755f2002d7cc0e84ca6', 0, '2020-04-05 10:08:07'),
('7a7b5a3984a557b840f9b723cf84867617f3516fb80fdf4d9c3b947fbc45240bc251e0dd30a8e8d1', 'eec26f1b7eeec9d0178f6307cd3b9a76740f494b75f00d45bfc7ed05df66226a9d3a30f578c05f27', 0, '2020-04-05 10:20:16'),
('7b2b8de509cb5e431da483be2fabbad000b9d57582eaac9eaea66bf0c8c57376bf92b5214c4ae36e', '18018a10bb23935b8f561c40ef9afb3beaeb6ebe8dfa91ac12ae71be5f7a905f4bd03f7d182ec406', 0, '2020-04-05 14:35:07'),
('988541e4cdf2d320d3baa5ab694666045b39bfb4db0442ce0ccef52abf28b13906f4f36f7bfda5c3', 'a975d13f605bbecaa8929cbcba79fa85ea39355ba03cfb2cfbbe5acc6d01e30eb022ae5a44d76439', 0, '2020-04-05 14:36:43'),
('98dc4b7b6bd154d7e67ebb97c514c71a7d6ea999f553c4a5708e8aa896ef97cc74209954287d7ef8', 'a79ded06519d1d64eb86601d3d2d1bdf5dc405dbf867e7acc18b954ed8a75900958525b614c92f2b', 0, '2020-04-05 10:08:16'),
('ae33522d390fe8da4ff6b905b2c21090a101803fb60f3faa09e18d822ba1d3153b76db7dca227dc9', '3fe6f02b26df37f9200a2aba34d0f552906492e0fb5d7f0fbe66a08c3166f276a4b5805e2ea0506d', 0, '2020-04-05 10:29:12'),
('bcab559313b2dcc2acbc91c8ceed4bf0cda788300f234b40ea1a3159edecaa277e4a5e601a192abb', '80f6ab6028d4028958dddd3d8d20fa78e9895fe2ef8df1e128effe1f5b7be6771ff563ccbdfc6855', 0, '2020-04-05 08:48:58'),
('ccf4fbbbbad9cb57ed8fe56d16fa72affcc93e4f8e3e1cd7640f7f1f9d82cca53d4372f3813516f7', '421dc770e1043410fafb772e80a9d3de6577da52ea0bc2a6754f3b7babdf04948439249c7f799575', 0, '2020-04-06 08:12:26'),
('e76f176a37a8399b901757e0ee9370d9d60571a17ad21e6492a8f15372583bc1129132f2e0024211', 'eaeb297c332259c1f69ed32f94f003407efcdc3b399acd90692dab45fe23c9c2ea317fdeb23d3162', 0, '2020-04-05 10:20:24'),
('ed1238e36176eb30ba86e669d55768cd85fbe0cdd72a01686f8f7cc5636356d7a98bea5c10f3f735', '831b0c8194b83d656f07312db7887f9877bc54c3e7085ccb7e71c743acd228ff7fbd07ddf00f9d99', 0, '2020-04-05 10:15:49'),
('fbb59217102f307b4f75d7c3f5ad5c6b5383886e01042a19dea1e1a2c2d80dd49809f4cec6c328ef', '5126977d97d9290b87d21ede8d3358bf39f4f7071e0e569e63a8c1c66eb8229cc368f40b9cfb7425', 0, '2020-04-05 13:24:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_detail_pinjam`
--

CREATE TABLE `tbl_detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_detail_pinjam`
--

INSERT INTO `tbl_detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 5),
(4, 1, 2),
(5, 1, 8),
(6, 1, 20),
(7, 1, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventaris`
--

CREATE TABLE `tbl_inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(10) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_inventaris`
--

INSERT INTO `tbl_inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(1, 'Kabel RJ 45', 'Bagus', 'Kabel untuk membuat jaringan komputer', 50, 2, '2019-04-03', 1, 'KBLRJ45', 2),
(2, 'Buku Sejarah Kelas 2', 'Bagus', 'Buku paket sejarah kelas 2', 50, 3, '2019-04-03', 2, 'SJRKLS2', 2),
(3, 'Terminal Kuningan', 'Bagus', 'Terminal', 20, 1, '2019-04-03', 1, 'TRMKNG', 2),
(4, 'Crim Tool', 'Bagus', 'Untuk jaringan', 10, 2, '2019-04-04', 1, 'CRMTL', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jenis`
--

CREATE TABLE `tbl_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(10) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_jenis`
--

INSERT INTO `tbl_jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Elektronik', 'ELC', 'Barang Elektronik'),
(2, 'Jaringan', 'JRG', 'Peralatan Jaringan'),
(3, 'Buku', 'BKU', 'Buku'),
(4, 'Alat Olahraga', 'ORG', 'Olahraga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_level`
--

CREATE TABLE `tbl_level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_level`
--

INSERT INTO `tbl_level` (`id_level`, `nama_level`) VALUES
(1, 'Admin'),
(2, 'Operator'),
(3, 'Peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pegawai`
--

CREATE TABLE `tbl_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'Ahmad', '1234', 'Cimahi'),
(2, 'Hasya', '789149', 'Bandung'),
(3, 'Hafizh', '3818012', 'Bandung'),
(4, 'Jack', '7492387', 'Barros'),
(5, 'John', '7183920', 'Cimindi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_peminjaman`
--

CREATE TABLE `tbl_peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `status_peminjaman` tinyint(1) NOT NULL DEFAULT '0',
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_peminjaman`
--

INSERT INTO `tbl_peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(1, '2019-04-04', '2019-04-04', 0, 1),
(2, '2019-04-04', '2019-04-04', 0, 1),
(3, '2019-04-04', '2019-04-04', 1, 2),
(4, '2019-04-04', '2019-04-05', 1, 1),
(5, '2019-04-04', '2019-04-04', 0, 2),
(6, '2019-04-06', '2019-04-06', 0, 1),
(7, '2019-04-08', '2019-04-08', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_petugas`
--

CREATE TABLE `tbl_petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `remember_token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_petugas`
--

INSERT INTO `tbl_petugas` (`id_petugas`, `username`, `password`, `nip`, `alamat`, `nama_petugas`, `id_level`, `remember_token`) VALUES
(2, 'admin', '$2y$10$yBFk1qjQ0/EwQeeT/OFBo.L4bLyct24Hfn2HLkEadGGZCP1lSbtr6', '1234', 'Cimahi', 'Ahmad ', 1, ''),
(3, 'operator', '$2y$10$yBFk1qjQ0/EwQeeT/OFBo.L4bLyct24Hfn2HLkEadGGZCP1lSbtr6', '5678', 'Cimahi', 'Hasya', 2, ''),
(4, 'peminjam', '$2y$10$yBFk1qjQ0/EwQeeT/OFBo.L4bLyct24Hfn2HLkEadGGZCP1lSbtr6', '9011', 'Cimahi', 'Hafizh', 3, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ruang`
--

CREATE TABLE `tbl_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_ruang`
--

INSERT INTO `tbl_ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Laboratorium', 'LAB', 'Ruang Laboratorium'),
(2, 'Perpustakaan', 'PPS', 'Perpustakaan'),
(3, 'WS RPL', 'RPL', 'Workshop'),
(4, 'YAYA', 'TES', 'dasda');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indeks untuk tabel `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indeks untuk tabel `tbl_detail_pinjam`
--
ALTER TABLE `tbl_detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`,`id_inventaris`);

--
-- Indeks untuk tabel `tbl_inventaris`
--
ALTER TABLE `tbl_inventaris`
  ADD PRIMARY KEY (`id_inventaris`);

--
-- Indeks untuk tabel `tbl_jenis`
--
ALTER TABLE `tbl_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `tbl_level`
--
ALTER TABLE `tbl_level`
  ADD PRIMARY KEY (`id_level`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `tbl_peminjaman`
--
ALTER TABLE `tbl_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indeks untuk tabel `tbl_petugas`
--
ALTER TABLE `tbl_petugas`
  ADD PRIMARY KEY (`username`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indeks untuk tabel `tbl_ruang`
--
ALTER TABLE `tbl_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_inventaris`
--
ALTER TABLE `tbl_inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_jenis`
--
ALTER TABLE `tbl_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_peminjaman`
--
ALTER TABLE `tbl_peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_petugas`
--
ALTER TABLE `tbl_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_ruang`
--
ALTER TABLE `tbl_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
