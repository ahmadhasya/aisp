<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"HomeController@index");

Route::post('/auth',"HomeController@login");

Auth::routes();

Route::get('/getUser',"HomeController@getUser");

Route::post('/addInventaris',"CTRLInventarisir@addInventaris");
Route::get('/deleteInventaris/{id}',"CTRLInventarisir@deleteInventaris");
Route::get('/getInventaris/{id}',"CTRLInventarisir@getInventaris");
Route::get('/allInventaris',"CTRLInventarisir@allInventaris");

Route::post('/addJenisBarang',"CTRLInventarisir@addJenisBarang");
Route::get('/deleteJenisBarang/{id}',"CTRLInventarisir@deleteJenisBarang");
Route::get('/getJenisBarang/{id}',"CTRLInventarisir@getJenisBarang");
Route::get('/allJenisBarang',"CTRLInventarisir@allJenisBarang");

Route::post('/addRuang',"CTRLInventarisir@addRuang");
Route::get('/deleteRuang/{id}',"CTRLInventarisir@deleteRuang");
Route::get('/getRuang/{id}',"CTRLInventarisir@getRuang");
Route::get('/allRuang',"CTRLInventarisir@allRuang");

Route::get('/allPeminjaman',"CTRLPeminjaman@allPeminjaman");
Route::get('/allPegawai',"CTRLPeminjaman@allPegawai");
Route::get('/getPeminjaman/{id}',"CTRLPeminjaman@getPeminjaman");
Route::post('/addPeminjaman',"CTRLPeminjaman@addPeminjaman");

Route::get('/allPengembalian',"CTRLPengembalian@allPengembalian");
Route::get('/Pengembalian/{id}',"CTRLPengembalian@Pengembalian");
Route::get('/getPengembalian/{id}',"CTRLPengembalian@getPengembalian");

Route::get("/generateLaporan","CTRLGenerateLaporan@generateLaporan");
Route::get("/laporanPeminjaman/{mulai}/{akhir}","CTRLGenerateLaporan@laporanPeminjaman");

Route::get('/home', 'HomeController@index')->name('home');
