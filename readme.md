# Aplikasi Inventaris Sarana Prasarana SMK

# Apa AISP
AISP adalah aplikasi untuk mencatat inventaris dan kegiatan meminjam inventaris.

# Syarat yang harus dipenuhi

1. PHP versi 7.2
2. Harus sudah terinstall git. (Untuk nge-clone projek)
3. DBMS MySQL(Config db nya sudah pake mysql, kalo yang lain ya seterah.)
4. XAMPP
5. Composer (Untuk pasang vendor. Cari aja di gugel)
6. Git


# Cara install
1. Di komputernya harus sudah terinstall git. Buat apa ? Buat ngambil source projek.

	`git clone https://gitlab.com/ahmadhasya/aisp.git`

2. Masuk ke projek kemudian command

	`composer install`

3. Import file database "db_ujikom.sql"
4. Duplicate file ".env.example" menjadi ".env". Ubah file ".env" sesuai dengan pengaturan databasenya.
5. Command yang dibawah untuk menyalakan server.

	`php artisan serve`

6. Untuk akun yang sudah terdaftar yaitu

```
	{
		"username" : "admin",
		"password" : "123"
	}
```

# Note
Aplikasi ini merupakan penunjang untuk kegiatan Uji Kompetensi. Semoga semuanya lulus dan kalau ada bug bikinin issue euy.

By Ahmad Hasya
